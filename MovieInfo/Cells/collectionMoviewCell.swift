//
//  collectionMoviewCell.swift
//  MovieInfo
//
//  Created by Shireesh M Shukla on 05/09/20.
//  Copyright © 2020 Alfian Losari. All rights reserved.
//

import UIKit
import Cards
import Kingfisher

class collectionMoviewCell: UICollectionViewCell,CardDelegate {
    @IBOutlet var movieVw: CardHighlight!
    override func awakeFromNib() {
        super.awakeFromNib()
        movieVw.delegate = self
        movieVw.hasParallax = true
        movieVw.buttonText = nil
        
    }
    func configure(viewModel: MovieViewViewModel) {
        
        movieVw.title = viewModel.title
        
        if let image = getImage(from: viewModel.posterURL) {
            //5. Apply image
            movieVw.backgroundImage = image
        }
        else{
            movieVw.backgroundImage = nil
        }
        movieVw.icon = nil
        movieVw.itemTitle = viewModel.overview
        movieVw.itemSubtitle = ""
        movieVw.movieID = viewModel.movieID
    }
    func getImage(from string: URL) -> UIImage? {
        //2. Get valid URL
        
        
        var image: UIImage? = nil
        do {
            //3. Get valid data
            let data = try Data(contentsOf: string, options: [])
            
            //4. Make image
            image = UIImage(data: data)
        }
        catch {
            print(error.localizedDescription)
        }
        
        return image
    }
    
}
