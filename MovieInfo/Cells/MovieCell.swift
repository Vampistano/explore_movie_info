//
//  MovieCell.swift
//  MovieInfo
//
//  Created by Shireesh M Shukla on 01/09/20.
//  Copyright © 2020 Shireesh M Shukla. All rights reserved.
//

import UIKit
import Kingfisher
import Cards
import CoreData
class MovieCell: UITableViewCell,CardDelegate {
    var open = false
    var isSearch = false
    var viewModel: MovieViewViewModel!
    @IBOutlet var movieVw: CardHighlight!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        movieVw.delegate = self
        movieVw.hasParallax = true
        movieVw.buttonText = "Book"
        
        
    }
    func configure(viewModel: MovieViewViewModel) {
        
        movieVw.title = viewModel.title
        if let image = getImage(from: viewModel.posterURL) {
            //5. Apply image
            movieVw.backgroundImage = image
            
        }
        else{
            movieVw.backgroundImage = nil
        }
        movieVw.icon = nil
        movieVw.itemTitle = viewModel.overview
        movieVw.itemSubtitle = ""
        movieVw.movieID = viewModel.movieID
    }
    func getImage(from string: URL) -> UIImage? {
        //2. Get valid URL
        
        
        var image: UIImage? = nil
        do {
            //3. Get valid data
            let data = try Data(contentsOf: string, options: [])
            
            //4. Make image
            image = UIImage(data: data)
        }
        catch {
            print(error.localizedDescription)
        }
        
        return image
    }
    func cardDidCloseDetailView(card: Card) {
        print("here")
        self.open = false
        movieVw.buttonText = "Book"
    }
    func cardDidShowDetailView(card: Card) {
        self.open = true
        movieVw.buttonText = nil
    }
    
    func cardHighlightDidTapButton(card: CardHighlight, button: UIButton) {
        
        
        
        if card == self.movieVw {
            
            if  !self.open {
                if self.isSearch{
                    self.syncCaching()
                }
                card.buttonText = nil
                NotificationCenter.default.post(name: .onReload, object: nil,userInfo: ["title" : card.title , "overview" : card.itemTitle, "id" : card.movieID])
                card.open()
            }
        }
    }
    
    fileprivate func syncCaching(){
        if UserDefaults.standard.object(forKey: "moviesCache") != nil{
            var stored = false
            var cacheMovies: [MovieViewViewModel] = UserDefaults.standard.structArrayData(MovieViewViewModel.self, forKey: "moviesCache")
            
            if cacheMovies.count == 5{
                for (index,_) in cacheMovies.enumerated(){
                    if cacheMovies[index].movieID == self.viewModel.movieID{
                        stored = true
                        break
                    }
                }
                if !stored{
                    cacheMovies.remove(at: 0)
                    cacheMovies.append(self.viewModel)
                    UserDefaults.standard.removeObject(forKey: "moviesCache")
                    UserDefaults.standard.setStructArray(cacheMovies, forKey: "moviesCache")
                    UserDefaults.standard.synchronize()
                }
            }
            else{
                cacheMovies.append(self.viewModel)
                UserDefaults.standard.removeObject(forKey: "moviesCache")
                UserDefaults.standard.setStructArray(cacheMovies, forKey: "moviesCache")
                UserDefaults.standard.synchronize()
            }
        }else{
            UserDefaults.standard.setStructArray([self.viewModel], forKey: "moviesCache")
            UserDefaults.standard.synchronize()
        }
        
    }
    
}
