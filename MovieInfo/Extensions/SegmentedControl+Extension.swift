//
//  SegmentedControl+Extension.swift
//  MovieInfo
//
//  Created by Shireesh M Shukla on 01/09/20.
//  Copyright © 2020 Shireesh M Shukla. All rights reserved.
//

import UIKit

extension UISegmentedControl {
    
    // this way we can send the user action of segment control for category type by user itself and communicate back to our End Point class for CustomStringConvertible, CaseIterable
    var endpoint: Endpoint {
        switch self.selectedSegmentIndex {
        case 0: return .nowPlaying
        case 1: return .popular
        case 2: return .upcoming
        case 3: return .topRated
        default: fatalError()
        }
    }
}
// Custom Extension of user defaults for caching and to store our Model struct in array .
extension UserDefaults {
    open func setStructArray<T: Codable>(_ value: [T], forKey defaultName: String){
         // first we will have our encoded value in data and higher functions to have ease in generic array
        let data = value.map { try? JSONEncoder().encode($0) }
         // setting up the set method of user defaults for given struct array
        set(data, forKey: defaultName)
    }
    
    open func structArrayData<T>(_ type: T.Type, forKey defaultName: String) -> [T] where T : Decodable {
        guard let encodedData = array(forKey: defaultName) as? [Data] else {
            return []
        }
        // returning the struct Data Array of given key
        return encodedData.map { try! JSONDecoder().decode(type, from: $0) }
    }
}
