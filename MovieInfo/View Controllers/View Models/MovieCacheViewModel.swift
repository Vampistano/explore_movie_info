//
//  MovieCacheViewModel.swift
//  MovieInfo
//
//  Created by Shireesh M Shukla on 06/09/20.
//  Copyright © 2020 Alfian Losari. All rights reserved.
//

import Foundation
public class MovieCacheViewModel: NSObject, NSCoding {
    
    public var movies: [MovieCache] = []
    
    enum Key:String {
        case movies = "movies"
    }
    
    init(movies: [MovieCache]) {
        self.movies = movies
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(movies, forKey: Key.movies.rawValue)
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        let mRanges = aDecoder.decodeObject(forKey: Key.movies.rawValue) as! [MovieCache]
        
        self.init(movies: mRanges)
    }
    
    
}
public class MovieCache: NSObject, NSCoding {
    
    public var movie: Movie

    public static let dateFormatter: DateFormatter = {
        $0.dateStyle = .medium
        $0.timeStyle = .none
        return $0
    }(DateFormatter())
    
    
   
    
    var title: String {
        return movie.title
    }
    
    var overview: String {
        return movie.overview
    }
    
    var posterURL: URL {
        return movie.backdropURL
    }
    
    var movieID: String {
        return String(movie.id)
    }
    
//    var releaseDate: String {
//        return MovieCache.dateFormatter.string(from: movie.releaseDate)
//    }
    
    var rating: String {
        let rating = Int(movie.voteAverage)
        let ratingText = (0..<rating).reduce("") { (acc, _) -> String in
            return acc + "⭐️"
        }
        return ratingText
    }
    
    enum Key:String {
        case movie = "movie"
    }
    init(movies: Movie) {
           self.movie = movies
        super.init()
       }
    
    
    
    
    public func encode(with aCoder: NSCoder) {
        
        aCoder.encode(movie, forKey: Key.movie.rawValue)
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        
        let mlocation = aDecoder.decodeObject(forKey: Key.movie.rawValue)
        
      
        self.init(movies: mlocation as! Movie)
    }
}
