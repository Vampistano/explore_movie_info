//
//  MovieViewViewModel.swift
//  MovieInfo
//
//  Created by Shireesh M Shukla on 01/09/20.
//  Copyright © 2020 Shireesh M Shukla. All rights reserved.
//

import Foundation

public struct MovieViewViewModel: Codable {
    
    public var movie: Movie

    private static let dateFormatter: DateFormatter = {
        $0.dateStyle = .medium
        $0.timeStyle = .none
        return $0
    }(DateFormatter())
    
    
    init(movie: Movie) {
        self.movie = movie
    }
    
    var title: String {
        return movie.title
    }
    
    var overview: String {
        return movie.overview
    }
    
    var posterURL: URL {
        return movie.backdropURL
    }
    
    var movieID: String {
        return String(movie.id)
    }
    
//    var releaseDate: String {
//        return MovieViewViewModel.dateFormatter.string(from: movie.releaseDate)
//    }
    
    var rating: String {
        let rating = Int(movie.voteAverage)
        let ratingText = (0..<rating).reduce("") { (acc, _) -> String in
            return acc + "⭐️"
        }
        return ratingText
    }
    
}
