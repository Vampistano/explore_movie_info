//
//  DetailViewController.swift
//  Demo
//
//  Created by Shireesh M Shukla on 01/09/20.
//  Copyright © 2020 Shireesh M Shukla. All rights reserved.
//

import UIKit
import Cards
import Alamofire
import SwiftyJSON
import RxSwift
import RxCocoa

// Using NSNotificationCenter for communication for update .Though Here Delegate should be the best example of communication from one to one,but preferred to stay with this as we can more use of it .
extension Notification.Name {
    static let  onReload = Notification.Name("onReload")
}


extension MovieDetailViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // return the array for title of recommended moviesa
        return arrTitle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath)
        let card = cell.viewWithTag(1000) as! CardArticle
        
        if arrTitle.count > 0{
            card.title = arrTitle[indexPath.row]
            if let image = getImage(from: arrBackdropPath[indexPath.row]) {
                card.backgroundImage = image
            }
        }
        return cell
    }
}
class MovieDetailViewController: UIViewController {
    
    
    @IBOutlet var bookButton: UIButton!
    
    
    func getImage(from string: String) -> UIImage? {
        //2. Get valid URL
        guard let url = URL(string: string)
            else {
                print("Unable to create URL")
                return nil
        }
        
        var image: UIImage? = nil
        do {
            //3. Get valid data
            let data = try Data(contentsOf: url, options: [])
            
            //4. Make image
            image = UIImage(data: data)
        }
        catch {
            print(error.localizedDescription)
        }
        
        return image
    }
    
    @IBOutlet var similarMovieVw: UICollectionView!
    var arrTitle :[String] = []
    var arrOriginalTitle :[String] = []
    
    var arrId :[String] = []
    var arrOverview :[String] = []
    var arrPosterPath :[String] = []
    var arrBackdropPath :[String] = []
    
    
    @IBOutlet var titleOfMovie: UILabel!
    @IBOutlet var DescriptionLbl: UILabel!
    @IBOutlet var overviewOfMovie: UILabel!
    let colors = [
        UIColor.red,
        UIColor.yellow,
        UIColor.blue,
        UIColor.green,
        UIColor.brown,
        UIColor.purple,
        UIColor.orange
    ]
    
    override func viewDidLoad() {
        print("Loaded!")
        NotificationCenter.default.addObserver(self, selector: #selector(combineData(notfication:)), name: .onReload, object: nil)
        self.similarMovieVw.delegate = self
        self.similarMovieVw.dataSource = self
        bookButton.layer.cornerRadius = 18
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.resetAttributes()
    }
    func resetAttributes(){
        self.arrTitle.removeAll()
         self.arrOriginalTitle.removeAll()
         self.arrId.removeAll()
         self.arrOverview.removeAll()
        self.arrPosterPath.removeAll()
        self.arrBackdropPath.removeAll()
        
    }
    @objc func combineData(notfication: NSNotification){
        guard let titleOfMovie = notfication.userInfo?["title"] as? String ,let _ = notfication.userInfo?["overview"] as? String,let id = notfication.userInfo?["id"] as? String else{
            return
        }
        self.GetSimilarMovies(id: id)
        
        Alamofire.request(  "https://api.themoviedb.org/3/movie/\(id)?api_key=18ef48e8b61611c64f2b72a39e7d277b",method: .get, encoding: JSONEncoding.default).responseJSON(completionHandler: { (responseData) -> Void in
            
            self.view.isUserInteractionEnabled = true
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                self.titleOfMovie.text = titleOfMovie
                self.overviewOfMovie.text = swiftyJsonVar["tagline"].stringValue
                self.DescriptionLbl.text = swiftyJsonVar["overview"].stringValue
                print("https://api.themoviedb.org/3/movie/\(id))?api_key=18ef48e8b61611c64f2b72a39e7d277b")
            }
        })
    }
    
    
    func GetSimilarMovies(id:String){
        Alamofire.request(  "https://api.themoviedb.org/3/movie/\(id)/similar?api_key=18ef48e8b61611c64f2b72a39e7d277b&page=1",method: .get, encoding: JSONEncoding.default).responseJSON(completionHandler: { (responseData) -> Void in
            
            self.view.isUserInteractionEnabled = true
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                print("https://api.themoviedb.org/3/movie/\(id)/similar?api_key=18ef48e8b61611c64f2b72a39e7d277b")
                let arr = swiftyJsonVar["results"].arrayValue
                for i in 0...arr.count {
                    if i < arr.count{
                        self.arrTitle.append(arr[i]["title"].stringValue)
                        self.arrOriginalTitle.append(arr[i]["original_title"].stringValue)
                        self.arrId.append(arr[i]["id"].stringValue)
                        self.arrOverview.append(arr[i]["overview"].stringValue)
                        self.arrPosterPath.append("https://image.tmdb.org/t/p/w500\(arr[i]["poster_path"].stringValue)")
                        self.arrBackdropPath.append("https://image.tmdb.org/t/p/w500\(arr[i]["backdrop_path"].stringValue)")
                    }
                    else{
                        self.similarMovieVw.reloadData()
                    }
                    
                }
            }
        })
    }
    
}

