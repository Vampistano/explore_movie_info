//
//  MovieSearchViewController.swift
//  MovieInfo
//
//  Created by Shireesh M Shukla on 01/09/20.
//  Copyright © 2020 Shireesh M Shukla. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Cards
import  CoreData
class MovieSearchViewController: UIViewController {
    
    @IBOutlet var tbleViewUpperConstant: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet var slidingVw: CardGroupSliding!
    var isSearchCanceled = false
    @IBOutlet var upperVw: UICollectionView!
    @IBOutlet var header: UILabel!
    var movieSearchViewViewModel: MovieSearchViewViewModel!
    let disposeBag = DisposeBag()
    var recentSearches: [MovieViewViewModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        let searchBar = self.navigationItem.searchController!.searchBar
        
        movieSearchViewViewModel = MovieSearchViewViewModel(query: searchBar.rx.text.orEmpty.asDriver(), movieService: MovieStore.shared)
        
        movieSearchViewViewModel.movies.drive(onNext: {[unowned self] (_) in
            self.isSearchCanceled = false
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
        
       
        movieSearchViewViewModel.isFetching.drive(activityIndicatorView.rx.isAnimating)
            .disposed(by: disposeBag)
    
        movieSearchViewViewModel.info.drive(onNext: {[unowned self] (info) in
            self.infoLabel.isHidden = !self.movieSearchViewViewModel.hasInfo
            self.infoLabel.text = info
        }).disposed(by: disposeBag)
        
        searchBar.rx.searchButtonClicked
            .asDriver(onErrorJustReturn: ())
            .drive(onNext: { [unowned searchBar] in
                searchBar.resignFirstResponder()
                
            }).disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked
            .asDriver(onErrorJustReturn: ())
            .drive(onNext: { [unowned searchBar] in
                searchBar.resignFirstResponder()
                self.isSearchCanceled = true
                 self.tableView.reloadData()
            }).disposed(by: disposeBag)

        setupTableView()
        
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refreshCacheStatus()
    }
    func refreshCacheStatus(){
        if UserDefaults.standard.object(forKey: "moviesCache") != nil{
            let decoded  =  UserDefaults.standard.structArrayData(MovieViewViewModel.self, forKey: "moviesCache")
            self.recentSearches = decoded
            self.upperVw.reloadData()
            self.header.isHidden = false
                      self.upperVw.isHidden = false
            self.tbleViewUpperConstant.constant = 22
        }
        else{
            self.header.isHidden = true
            self.upperVw.isHidden = true
            self.tbleViewUpperConstant.constant = -191
        }
    }
  
    private func setupNavigationBar() {
        navigationItem.searchController = UISearchController(searchResultsController: nil)
        self.definesPresentationContext = true
        navigationItem.searchController?.dimsBackgroundDuringPresentation = false
        navigationItem.searchController?.hidesNavigationBarDuringPresentation = false
        
        navigationItem.searchController?.searchBar.sizeToFit()
        navigationItem.hidesSearchBarWhenScrolling = true
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func setupTableView() {
        tableView.tableFooterView = UIView()
               tableView.estimatedRowHeight = 400
        tableView.register(UINib(nibName: "MovieCell", bundle: nil), forCellReuseIdentifier: "MovieCell")
    }
    
}

extension MovieSearchViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearchCanceled{
            return 0
        }
        return movieSearchViewViewModel.numberOfMovies
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as! MovieCell
        let card = cell.viewWithTag(1000) as!  CardHighlight
        cell.isSearch = true
        
                      let cardContent = storyboard!.instantiateViewController(withIdentifier: "CardContent")

               if let viewModel = movieSearchViewViewModel.viewModelForMovie(at: indexPath.row) {
                   cell.viewModel = viewModel
                   cell.configure(viewModel: viewModel)
               }
               card.shouldPresent(cardContent, from: self, fullscreen: false)
        return cell
    }
        
}
extension MovieSearchViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // return the array for title of recommended moviesa
        return self.recentSearches.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieSearchCell", for: indexPath) as! collectionMoviewCell

        if let viewModel :MovieViewViewModel = self.recentSearches[indexPath.row] {
                          cell.configure(viewModel: viewModel)
                      }
        return cell
    }
}
