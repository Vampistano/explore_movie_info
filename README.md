# Explore Movie Info #ExpressiveDisplay

![Alt text](./Promo-1.jpg?raw=true "Movie Info")
![Alt text](./Promo-2.jpg?raw=true "Movie Info")


## Getting Started

- Install Xcode > 11 
- Clone git clone https://Vampistano@bitbucket.org/Vampistano/explore_movie_info.git

OR

- Clone in xcode directly 

OR

- Find the attached Zip file for this project in mail .
- pod install .
- open MovieInfo.xcworkspace file .
- Hit Run


## Application Over View
- On first tab which is features for movies list and their detail info .
- By calling TheMovieDB for movies,we shows up list for 4 category like now Playing,Top Rated etc ,
- On Clicking on Book Button ,Selected Movie shows up with its detailed Content and with similiar movies display in
last.
- On second tab which is Search shows up  with recent searches and search to some limit and satisfies the all cases given for search,there is one thing we need to see the implementaion of Recent Searches which will stores only when user will got to detail or select any movie from the displayed searched List. 
### Once User will go to detail by tapping on book button on the respected searched list,then and only selected movie will be cached .

## Features
- Used MVVM as design architecure
- Used RXSwift,RXCocoa,Cards,SwiftyJSON,Alamofire 
- Card View Displayed List
- segment for four category Now Playing,TopRated,Upcoming
- Recent searches Cahed in user Defaluts
- for dynamic purposes uses TMDB searhc api with real data run time .


## Search Algorithim

For staic list screen here is the way for it .

first search string in faster way ** used Boyer and Moore  string search  algorithm
Please see for the file name ViewControllerForSearchLogic.swift

### especially look for index(of) method written for extension String in the same mentioned file .


Happy Coding!



best,
Shireesh

