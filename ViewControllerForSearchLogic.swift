//
//  ViewControllerForSearchLogic.swift
//  ExpressingView
//
//  Created by Shireesh M Shukla on 01/09/20.
//  Copyright © 2020 Shireesh M Shukla. All rights reserved.
//

import UIKit
import Cards
import Foundation
import Alamofire
import SwiftyJSON
import ContentLoader

extension ViewControllerForSearchLogic: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        if searchBar.text != ""{
            isSearching = true
            filterContentForSearchText(searchBar.text!)
        }
        else{
            isSearching = false
            self.filteredTitle.removeAll()
            self.filteredOriginalTitle.removeAll()
            self.filteredId.removeAll()
            self.filteredOverview.removeAll()
            self.filteredPosterPath.removeAll()
            self.filteredBackdropPath.removeAll()
            self.movieListVw.reloadData()
        }
        
    }
}

extension ViewControllerForSearchLogic: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        if searchBar.text != ""{
            isSearching = true
            filterContentForSearchText(searchBar.text!)
        }
        else{
            isSearching = false
            self.callApi()
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("Cancel")
    }
}

class ViewControllerForSearchLogic: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchControllerDelegate {
    
    
    var arrTitle :[String] = []
    var arrOriginalTitle :[String] = []
    var indexOfA = 0
    var indexOfB = 0
    
    var arrId :[String] = []
    var arrOverview :[String] = []
    var arrPosterPath :[String] = []
    var arrBackdropPath :[String] = []
    let searchController = UISearchController(searchResultsController: nil)
    var filteredTitle: [String] = []
    var filtered: [String] = []
    var isSearching = false
    var filteredOriginalTitle :[String] = []
    var filteredId :[String] = []
    var filteredOverview :[String] = []
    var filteredPosterPath :[String] = []
    var filteredBackdropPath :[String] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredTitle.count
        }
        return arrTitle.count
    }
    func callApi(){
        Alamofire.request(  "https://api.themoviedb.org/3/movie/now_playing?api_key=18ef48e8b61611c64f2b72a39e7d277b&language=en-US&page=1",method: .get, encoding: JSONEncoding.default).responseJSON(completionHandler: { (responseData) -> Void in
            
            self.view.isUserInteractionEnabled = true
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                let arr = swiftyJsonVar["results"].arrayValue
                for i in 0...arr.count {
                    if i < arr.count{
                        self.arrTitle.append(arr[i]["title"].stringValue)
                        self.arrOriginalTitle.append(arr[i]["original_title"].stringValue)
                        self.arrId.append(arr[i]["id"].stringValue)
                        self.arrOverview.append(arr[i]["overview"].stringValue)
                        self.arrPosterPath.append("https://image.tmdb.org/t/p/w500\(arr[i]["poster_path"].stringValue)")
                        self.arrBackdropPath.append("https://image.tmdb.org/t/p/w500\(arr[i]["backdrop_path"].stringValue)")
                    }
                    else{
                        self.movieListVw.reloadData()
                        self.movieListVw.hideLoading()
                    }
                    
                }
                self.filteredTitle.removeAll()
                self.filteredOriginalTitle.removeAll()
                self.filteredId.removeAll()
                self.filteredOverview.removeAll()
                self.filteredPosterPath.removeAll()
                self.filteredBackdropPath.removeAll()
            }
            
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoviecardCell") as! MoviecardCell
        let card = cell.viewWithTag(1000) as!  CardHighlight
        
        let cardContent = storyboard!.instantiateViewController(withIdentifier: "CardContent")
        if isSearching {
            if filteredTitle.count >= indexPath.row{
                card.title = filteredTitle[indexPath.row]
                if let image = getImage(from: filteredBackdropPath[indexPath.row]) {
                    //5. Apply image
                    card.backgroundImage = image
                    
                }
                card.icon = nil
                card.itemTitle = filteredOverview[indexPath.row]
                card.movieID = filteredId[indexPath.row]
                //            card.itemTitleSize = 30.0
                card.itemSubtitle = ""
            }
        } else {
            if arrTitle.count > 0{
                card.title = arrTitle[indexPath.row]
                if let image = getImage(from: arrBackdropPath[indexPath.row]) {
                    //5. Apply image
                    card.backgroundImage = image
                    
                }
                card.icon = nil
                card.itemTitle = arrOverview[indexPath.row]
                card.movieID = arrId[indexPath.row]
                //            card.itemTitleSize = 30.0
                card.itemSubtitle = ""
            }
        }
        
        
        card.shouldPresent(cardContent, from: self, fullscreen: true)
        return cell
    }
    func getImage(from string: String) -> UIImage? {
        //2. Get valid URL
        guard let url = URL(string: string)
            else {
                print("Unable to create URL")
                return nil
        }
        
        var image: UIImage? = nil
        do {
            //3. Get valid data
            let data = try Data(contentsOf: url, options: [])
            
            //4. Make image
            image = UIImage(data: data)
        }
        catch {
            print(error.localizedDescription)
        }
        
        return image
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Did")
        //
    }
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
        return searchController.isActive && isSearchBarEmpty
    }
    
    func filterContentForSearchText(_ searchText: String
    ) {
        let firstLetters = arrTitle.map { String($0.first!) }
        print(firstLetters) // ["G", "M"]
        filtered = arrTitle.filter { (id: String) -> Bool in
            
            let str = String(id.first!)
            
            
            if searchText.count > 1{
                if searchText == id{
                    self.filteredTitle.removeAll()
                    self.filteredOriginalTitle.removeAll()
                    self.filteredId.removeAll()
                    self.filteredOverview.removeAll()
                    self.filteredPosterPath.removeAll()
                    self.filteredBackdropPath.removeAll()
                    let dummy = arrTitle.map { String($0) }
                    indexOfB = dummy.indexes(of: id)[0]
                    print("\(arrTitle[indexOfB])")
                    if  self.filteredTitle.contains(arrTitle[indexOfB]) == false{
                        self.filteredTitle.append(arrTitle[indexOfB])
                    }
                    if  self.filteredOriginalTitle.contains(arrOriginalTitle[indexOfB]) == false{
                        self.filteredOriginalTitle.append(arrOriginalTitle[indexOfB])
                    }
                    if  self.filteredId.contains(arrId[indexOfB]) == false{
                        self.filteredId.append(arrId[indexOfB])
                        
                    }
                    if  self.filteredOverview.contains(arrOverview[indexOfB]) == false{
                        self.filteredOverview.append(arrOverview[indexOfB])
                        
                    }
                    if  self.filteredPosterPath.contains(arrPosterPath[indexOfB]) == false{
                        self.filteredPosterPath.append(arrPosterPath[indexOfB])
                        
                    }
                    if  self.filteredBackdropPath.contains(arrBackdropPath[indexOfB]) == false{
                        self.filteredBackdropPath.append(arrBackdropPath[indexOfB])
                        
                    }
                }
                else{
                    if (id.contains(searchText) == true){
                        self.filteredTitle.removeAll()
                        self.filteredOriginalTitle.removeAll()
                        self.filteredId.removeAll()
                        self.filteredOverview.removeAll()
                        self.filteredPosterPath.removeAll()
                        self.filteredBackdropPath.removeAll()
                        let dummyU = arrTitle.map { String($0) }
                        indexOfB = dummyU.indexes(of: id)[0]
                        let s = "\(dummyU[indexOfB])"
                        print("full array\(dummyU)")
                        print("full array index\(dummyU[indexOfB])")
                        print("matched\( s.indexOF(pattern: searchText)?.encodedOffset  ?? -1)")
                        let filteredStrings = arrTitle.filter({(item: String) -> Bool in
                            
                            
                            let stringMatch = item.lowercased().range(of: searchText.lowercased())
                            return stringMatch != nil ? true : false
                        })
                        print("string",filteredStrings)
                        let dummy = filteredStrings.map { String($0) }
                        
                        for i in 0...dummy.count-1 {
                            let val = dummy[i]
                            if val.prefix(searchText.count).lowercased() == searchText.lowercased() {
                                print("I found \(dummy[i])") // I found He
                                let folk = arrTitle.map { String($0) }
                                indexOfB = folk.indexes(of: dummy[i])[0]
                                if  self.filteredTitle.contains(arrTitle[indexOfB]) == false{
                                    self.filteredTitle.append(arrTitle[indexOfB])
                                }
                                if  self.filteredOriginalTitle.contains(arrOriginalTitle[indexOfB]) == false{
                                    self.filteredOriginalTitle.append(arrOriginalTitle[indexOfB])
                                }
                                if  self.filteredId.contains(arrId[indexOfB]) == false{
                                    self.filteredId.append(arrId[indexOfB])
                                    
                                }
                                if  self.filteredOverview.contains(arrOverview[indexOfB]) == false{
                                    self.filteredOverview.append(arrOverview[indexOfB])
                                    
                                }
                                if  self.filteredPosterPath.contains(arrPosterPath[indexOfB]) == false{
                                    self.filteredPosterPath.append(arrPosterPath[indexOfB])
                                    
                                }
                                if  self.filteredBackdropPath.contains(arrBackdropPath[indexOfB]) == false{
                                    self.filteredBackdropPath.append(arrBackdropPath[indexOfB])
                                    
                                }
                            }
                        }
                        
                    }
                    
                }
            }
            else{
                if searchText.uppercased() == str{
                    if firstLetters.contains(str){
                        print("\(id)")
                        indexOfA = firstLetters.firstIndex(of: str)!
                        let dummy = arrTitle.map { String($0) }
                        indexOfB = dummy.indexes(of: id)[0]
                        print("\(arrTitle[indexOfB])")
                        if  self.filteredTitle.contains(arrTitle[indexOfB]) == false{
                            self.filteredTitle.append(arrTitle[indexOfB])
                        }
                        if  self.filteredOriginalTitle.contains(arrOriginalTitle[indexOfB]) == false{
                            self.filteredOriginalTitle.append(arrOriginalTitle[indexOfB])
                        }
                        if  self.filteredId.contains(arrId[indexOfB]) == false{
                            self.filteredId.append(arrId[indexOfB])
                            
                        }
                        if  self.filteredOverview.contains(arrOverview[indexOfB]) == false{
                            self.filteredOverview.append(arrOverview[indexOfB])
                            
                        }
                        if  self.filteredPosterPath.contains(arrPosterPath[indexOfB]) == false{
                            self.filteredPosterPath.append(arrPosterPath[indexOfB])
                            
                        }
                        if  self.filteredBackdropPath.contains(arrBackdropPath[indexOfB]) == false{
                            self.filteredBackdropPath.append(arrBackdropPath[indexOfB])
                            
                        }
                        
                        
                    }
                }
            }
            
            
            if isSearchBarEmpty {
                
                return true
            } else {
                return firstLetters.contains(str)
            }
        }
        
        movieListVw.reloadData()
    }
    
    func handleKeyboard(notification: Notification) {
        // 1
        guard notification.name == UIResponder.keyboardWillChangeFrameNotification else {
            //        searchFooterBottomConstraint.constant = 0
            view.layoutIfNeeded()
            return
        }
        
        guard
            let info = notification.userInfo,
            let keyboardFrame = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
            else {
                return
        }
        
        // 2
        let keyboardHeight = keyboardFrame.cgRectValue.size.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //        self.searchFooterBottomConstraint.constant = keyboardHeight
            self.view.layoutIfNeeded()
        })
    }
    @IBOutlet var movieListVw: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        movieListVw.delegate = self
        movieListVw.dataSource = self
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        // 2
        searchController.obscuresBackgroundDuringPresentation = false
        // 3
        searchController.searchBar.placeholder = "Search Moviess"
        // 4
        navigationItem.searchController = searchController
        // 5
        definesPresentationContext = true
        navigationItem.hidesSearchBarWhenScrolling = true
        
        var format = ContentLoaderFormat()
        format.color = "#F6F6F6".hexColor
        format.radius = 5
        
        movieListVw.startLoading(format: format)
        self.callApi()
    }
    
    
}




extension Array where Element: Equatable {
    func indexes(of element: Element) -> [Int] {
        return self.enumerated().filter({ element == $0.element }).map({ $0.offset })
    }
}
extension String {
    func indexOF( pattern: String) -> Index? {
        // Cache the length of the search pattern because we're going to
        // use it a few times and it's expensive to calculate.
        let patternLength = pattern.count
        guard patternLength > 0, patternLength <= count else { return nil }
        
        // Make the skip table. This table determines how far we skip ahead
        // when a character from the pattern is found.
        var skipTable = [Character: Int]()
        for (i, c) in pattern.enumerated() {
            skipTable[c] = patternLength - i - 1
        }
        
        // This points at the last character in the pattern.
        let p = pattern.index(before: pattern.endIndex)
        let lastChar = pattern[p]
        
        // The pattern is scanned right-to-left, so skip ahead in the string by
        // the length of the pattern. (Minus 1 because startIndex already points
        // at the first character in the source string.)
        var i = index(startIndex, offsetBy: patternLength - 1)
        
        // This is a helper function that steps backwards through both strings
        // until we find a character that doesn’t match, or until we’ve reached
        // the beginning of the pattern.
        func backwards() -> Index? {
            var q = p
            var j = i
            while q > pattern.startIndex {
                j = index(before: j)
                q = index(before: q)
                if self[j] != pattern[q] { return nil }
            }
            return j
        }
        
        // The main loop. Keep going until the end of the string is reached.
        while i < endIndex {
            let c = self[i]
            
            // Does the current character match the last character from the pattern?
            if c == lastChar {
                
                // There is a possible match. Do a brute-force search backwards.
                if let k = backwards() { return k }
                
                // If no match, we can only safely skip one character ahead.
                i = index(after: i)
            } else {
                // The characters are not equal, so skip ahead. The amount to skip is
                // determined by the skip table. If the character is not present in the
                // pattern, we can skip ahead by the full pattern length. However, if
                // the character *is* present in the pattern, there may be a match up
                // ahead and we can't skip as far.
                i = index(i, offsetBy: skipTable[c] ?? patternLength, limitedBy: endIndex) ?? endIndex
            }
        }
        return nil
    }
}
